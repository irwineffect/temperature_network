use rppal::spi::{Bus, Mode, SlaveSelect, Spi};
use rppal::gpio::{Gpio};
use embedded_nrf24l01::{NRF24L01, Configuration};
use embedded_hal::prelude::*;
use std::io::{BufWriter, Write};
use chrono;
use influxdb2;
use influxdb2::models::DataPoint;
use futures;
use tokio;
use std::sync::mpsc;

#[derive(Copy, Clone)]
struct Measurement {
    pipe: u8,
    temperature: f32,
    humidity: f32,
    pressure: f32,
    timestamp: i64,
}

impl Measurement {
    fn new(pipe: u8, data: &[u8]) -> Self {
        let temperature = f32_from_be(&data[0..4]);
        let humidity = f32_from_be(&data[4..8]);
        let pressure = f32_from_be(&data[8..12]);

        Self {
            pipe: pipe,
            timestamp: get_ns_timestamp(),
            temperature,
            humidity,
            pressure,
        }
    }
}

impl Into<DataPoint> for Measurement {
    fn into(self) -> DataPoint {
        DataPoint::builder("measurement")
            .tag("location", u8::to_string(&self.pipe))
            .field("temperature", self.temperature as f64)
            .field("humidity", self.humidity as f64)
            .field("pressure", self.pressure as f64)
            .timestamp(self.timestamp)
            .build().unwrap()
    }
}

fn f32_from_be(be_bytes: &[u8]) -> f32 {
    assert!(be_bytes.len() == 4);
    let mut buffer: [u8; 4] = [0u8; 4];
    buffer.clone_from_slice(be_bytes);
    f32::from_be_bytes(buffer)
}

fn get_ns_timestamp() -> i64 {
    use std::time::SystemTime;
    use std::convert::TryInto;

    SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_nanos().try_into().unwrap()
}

fn influx_thread(recv: mpsc::Receiver<Measurement>) {
    let host = "http://timberland.lan:8086";
    let org = "home";
    let token = "a76rOLrVMZJem13VucGI0mnll3k-FhTo--tFjpwddEE9HeT8M4fcxqrIwN1BvyoA5ggXAaNRjsvYaj3YGpgOoQ==";
    let bucket = "environmental_sensors";
    let client = influxdb2::Client::new(host, org, token);

    let rt = tokio::runtime::Builder::new_current_thread().enable_all().build().unwrap();

    loop {
        let point = match recv.recv() {
            Ok(point) => point,
            Err(_) => break
        };


        rt.block_on( async {
            loop {
                let point: DataPoint = point.clone().into();
                let points = futures::stream::iter(vec! [ point ]);
                match client.write(bucket, points).await {
                    Ok(()) => {
                        println!("sent to influxdb");
                        break;
                    },
                    Err(e) => {
                        dbg!(e);
                        std::thread::sleep(std::time::Duration::from_secs(10u64));
                    }
                }
            }
        })
    }
}

fn main() {

    let (send,recv) = mpsc::channel();

    std::thread::spawn(move || {influx_thread(recv)});

    let log = std::fs::OpenOptions::new().create(true).append(true).open("log.csv").unwrap();
    let mut log = BufWriter::new(log);


    let mut cs_pin = Gpio::new().unwrap().get(6).unwrap().into_output();
    cs_pin.set_high();
    let mut ce_pin = Gpio::new().unwrap().get(5).unwrap().into_output();
    ce_pin.set_low();

    let spi = Spi::new(
        Bus::Spi0,
        SlaveSelect::Ss0,
        8_000_000,
        Mode::Mode0).unwrap();



    let mut nrf24 = NRF24L01::new(ce_pin, cs_pin, spi).unwrap();

    nrf24.set_frequency(13).unwrap();
    nrf24.set_pipes_rx_enable(&[true; 6]).unwrap();
    nrf24.set_pipes_rx_lengths(&[Some(12), Some(12), Some(12), Some(12), Some(12), Some(12)]).unwrap();

    let mut nrf24 = nrf24.rx().unwrap();

    let mut delay = rppal::hal::Delay::new();

    println!("initialization complete!");


    loop {

        let r = nrf24.can_read().unwrap();

        if let Some(pipe_number) = r {

            let payload: &[u8] = &nrf24.read().unwrap();

            let point = Measurement::new(pipe_number, payload);

            println!();
            println!("rx on pipe {}", point.pipe);
            println!("temperature: {}", point.temperature);
            println!("humidity: {}", point.humidity);
            println!("pressure: {}", point.pressure);

            let current_time = chrono::Local::now();
            write!(log, "{},{},{},{},{}\n", current_time, point.pipe, point.temperature, point.humidity, point.pressure).unwrap();
            log.flush().unwrap();

            // Reserve pipe 0 for doing debugging, don't log to influxdb
            if pipe_number > 0 {
                send.send(point).unwrap();
            }
        }
        else {
            delay.delay_ms(100u16);
        }
    }
}
