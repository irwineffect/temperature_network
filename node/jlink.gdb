#target extended-remote mint18-oryx:2331
target extended-remote poliwag.lan:2331

# Reset the target
monitor reset

# Enable semihosting
monitor semihosting enable

# Load firmware
load

monitor reset

set print asm-demangle on
set backtrace limit 32

# Set Breakpoints
break DefaultHandler
break HardFault
break rust_begin_unwind
#break main

monitor go
disconnect
quit

#continue
#stepi
