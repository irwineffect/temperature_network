#![no_std]
#![no_main]

// pick a panicking behavior
//extern crate panic_halt; // you can put a breakpoint on `rust_begin_unwind` to catch panics
//extern crate panic_abort; // requires nightly
// extern crate panic_itm; // logs messages over ITM; requires ITM support
extern crate panic_semihosting; // logs messages to the host stderr; requires a debugger

use rtt_target::{rprintln};

//use cortex_m::asm;
use cortex_m_rt::entry;

use stm32f1xx_hal as hal;
use stm32f1xx_hal::{
    prelude::*,
    pac,
    //timer::Timer,
};

use embedded_nrf24l01::{NRF24L01, Configuration};

use bme280::spi::BME280;

use shared_bus;

use asm_delay::AsmDelay;

// const someday!
fn get_addr() -> &'static [u8; 5] {
    const ADDRS: [[u8; 5]; 6] = [ 
        [0xE7, 0xE7, 0xE7, 0xE7, 0xE7],
        [0xC2, 0xC2, 0xC2, 0xC2, 0xC2],
        [0xC3, 0xC2, 0xC2, 0xC2, 0xC2],
        [0xC4, 0xC2, 0xC2, 0xC2, 0xC2],
        [0xC5, 0xC2, 0xC2, 0xC2, 0xC2],
        [0xC6, 0xC2, 0xC2, 0xC2, 0xC2],
    ];

    let addr_index: usize = env!("ADDR").parse().unwrap();
    &ADDRS[addr_index]
}


#[entry]
fn main() -> ! {
    let cp = cortex_m::Peripherals::take().unwrap();

    let dp = pac::Peripherals::take().unwrap();

    let mut flash = dp.FLASH.constrain();
    let rcc = dp.RCC.constrain();
    let clocks = rcc.cfgr.freeze(&mut flash.acr);
    let mut afio = dp.AFIO.constrain();

    rtt_target::rtt_init_print!();

    let mut gpioa = dp.GPIOA.split();
    let mut gpioc = dp.GPIOC.split();

    let mut led_pin = gpioc.pc13.into_push_pull_output(&mut gpioc.crh);

    // let uart_tx_pin = gpioa.pa9.into_alternate_push_pull(&mut gpioa.crh);
    // let uart_rx_pin = gpioa.pa10;
    // let uart1 = hal::serial::Serial::usart1(
    //         dp.USART1,
    //         (uart_tx_pin, uart_rx_pin),
    //         &mut afio.mapr,
    //         hal::serial::Config {
    //             baudrate: 9600.bps(),
    //             parity: hal::serial::Parity::ParityNone,
    //             stopbits: hal::serial::StopBits::STOP1
    //         },
    //         clocks,
    //         &mut rcc.apb2
    //         );

    let mut nrf24_ss_pin   = gpioa.pa4.into_push_pull_output(&mut gpioa.crl);
    nrf24_ss_pin.set_high();
    let mut bme280_ss_pin  = gpioa.pa1.into_push_pull_output(&mut gpioa.crl);
    bme280_ss_pin.set_high();

    let mosi_pin = gpioa.pa7.into_alternate_push_pull(&mut gpioa.crl);
    let miso_pin = gpioa.pa6;
    let sck_pin  = gpioa.pa5.into_alternate_push_pull(&mut gpioa.crl);
    let nrf24_ce_pin   = gpioa.pa2.into_push_pull_output(&mut gpioa.crl);
    //let irq_pin  = gpioa.pa3;

    let spi_pins = (
        sck_pin,
        miso_pin,
        mosi_pin
    );

    let spi = hal::spi::Spi::spi1(
        dp.SPI1,
        spi_pins,
        &mut afio.mapr,
        hal::spi::Mode {
            polarity: hal::spi::Polarity::IdleLow,
            phase: hal::spi::Phase::CaptureOnFirstTransition,
        },
        1.mhz(),
        clocks,
        );

    let spi_bus = shared_bus::BusManagerSimple::new(spi);

    let delay = AsmDelay::new(asm_delay::bitrate::Hertz(clocks.sysclk().0));
    let mut nrf24 = NRF24L01::new(nrf24_ce_pin, nrf24_ss_pin,
        spi_bus.acquire_spi()
    ).unwrap();
    nrf24.set_frequency(13).unwrap();
    let addr = get_addr();
    nrf24.set_tx_addr(addr).unwrap();
    nrf24.set_rx_addr(0, addr).unwrap();
    let mut nrf24 = Some(nrf24);

    let mut bme280 = BME280::new(
       spi_bus.acquire_spi(),
       bme280_ss_pin,
       delay,
    ).unwrap();
    bme280.init().unwrap();

    // Delay to wait for bme to warm up
    let mut delay = hal::delay::Delay::new(cp.SYST, clocks);
    delay.delay_ms(500u16);
    let _ = bme280.measure().unwrap();
    delay.delay_ms(500u16);

    // writeln!(uart1, "initialized");

    let mut a: u8 = 0;

    loop {
        led_pin.set_low();

        // measure temperature, pressure, and humidity
        let measurements = bme280.measure().unwrap();

        let mut tx = nrf24.take().unwrap().tx().unwrap();
        delay.delay_ms(1u16);

        if tx.can_send().unwrap() {
            let mut payload: [u8; 12] = [0u8; 12];
            payload[0..4].clone_from_slice(&measurements.temperature.to_be_bytes());
            payload[4..8].clone_from_slice(&measurements.humidity.to_be_bytes());
            payload[8..12].clone_from_slice(&measurements.pressure.to_be_bytes());

            rprintln!("{:#?}", payload);

            let payload = payload;
            tx.send(&payload).unwrap();
            tx.wait_empty().unwrap();
            rprintln!("data sent: {}", a);
        }
        else {
            rprintln!("can't send");
        }

        rprintln!("{:#?}", tx.observe().unwrap());

        nrf24 = Some(tx.standby().unwrap());
        led_pin.set_high();
        delay.delay_ms(5*60000u32);
        // delay.delay_ms(3000u32);
        a = a.wrapping_add(1);
    }
}
