# Home Temperature Network
The goal of this project is to develop a cheap temperature sensor network to be
able to monitor the temperature in several different rooms of the house. The
data is collected and logged by a central hub (raspberry pi). Eventually, I'd
like to make a web interface for viewing the data.

## Hardware
### Sensor Node
Microcontroller: STM32 Blue Pill
RF Tranceiver: NRF24L01+
Temperature Sensor: BME280

### Central Hub
Raspberry Pi (any version)
RF Tranceiver: NRF24L01+

